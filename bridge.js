const WebSocket = require('isomorphic-ws');
const PING_INTERVAL_MS = 15 * 1000;

class Bridge {
    constructor(args) {
        this.name = args.name;
        this.endpoint = args.endpoint;
        this.token = args.token;
        this.websocket = null;

        this.blocks = {};
    }

    // Bridge block definitions
    operation(opts, fun) {
        const block_id = opts.id;
        const block_message = opts.message;
        const block_args = opts.args;


        if (this.blocks[block_id]) {
            throw Error(`Block ${block_id} defined twice.`);
        }

        this.blocks[block_id] = {
            opts: {
                id: block_id,
                message: block_message,
                args: block_args,
                type: 'operation',
            },
            function: fun,
        };
    }

    getter(opts, fun) {
        const block_id = opts.id;
        const block_message = opts.message;
        const block_args = opts.args;
        const block_result_type = opts.result_type;


        if (this.blocks[block_id]) {
            throw Error(`Block ${block_id} defined twice.`);
        }

        this.blocks[block_id] = {
            opts: {
                id: block_id,
                message: block_message,
                args: block_args,
                type: 'getter',
                result_type: block_result_type,
            },
            function: fun,
        };
    }

    // Protocol handling
    _initialize() {

        // Send authentication
        this.websocket.send(JSON.stringify({
                "type": "AUTHENTICATION",
                "value": { "token": this.token }
        }))

        // Send configuration
        const blocks_defs = [];
        for (const block_id of Object.keys(this.blocks)) {
            const block = this.blocks[block_id];

            const argument_defs = [];
            for (const arg of block.opts.args) {
                argument_defs.push(arg);
            }

            blocks_defs.push({
                "id": block_id,
                "function_name": block_id,
                "block_type": block.opts.type,
                "block_result_type": block.opts.result_type || null,
                "message": block.opts.message,
                "arguments": argument_defs
            })
        }

        this.websocket.send(JSON.stringify({
            "type": "CONFIGURATION",
            "value": {
                "blocks": blocks_defs,
                "is_public": false,
                "service_name": this.name
            }
        }
        ))
    }

    _handle_function_call(event) {
        const message_id = event.message_id;
        const function_name = event.value.function_name;
        const args = event.value.arguments;

        this.blocks[function_name].function(...args, {})
            .then((result) => {
                if (result === undefined) {
                    result = null;
                }
                this.websocket.send(JSON.stringify({
                    "message_id": message_id,
                    "success": true,
                    "result": result,
                }));
            }).
            catch((err) => {
                console.error(err);
                this.websocket.send(JSON.stringify({
                    "message_id": message_id,
                    "success": false,
                }));
            });
    }

    _handle_service_registration(event) {
        // TODO: Support non-direct registrations
        this.websocket.send(JSON.stringify({
            message_id: event.message_id,
            success: true,
            result: null,
        }));
    }

    _handle_service_registration_confirmation(event) {
        this.websocket.send(JSON.stringify({
            message_id: event.message_id,
            success: true,
        }));
    }

    handle_event(event) {
        if (event.type == "FUNCTION_CALL") {
            this._handle_function_call(event);
        }
        else if (event.type == "GET_HOW_TO_SERVICE_REGISTRATION") {
            this._handle_service_registration(event);
        }
        else if (event.type == "REGISTRATION") {
            this._handle_service_registration_confirmation(event);
        }
        else {
            throw Error("Unknown event type");
        }
    }

    // Initialization
    run() {
        return new Promise((resolve, reject) => {
            this.websocket = new WebSocket(this.endpoint);

            this.websocket.onopen = () => {
                console.debug('Bridge connected, initializing');
                this._initialize();
                this._pingInterval = setInterval(() => {
                    this.websocket.ping();
                }, PING_INTERVAL_MS)
            };

            this.websocket.onclose = () => {
                console.debug('Bridge disconnected!');
                reject('disconnected');
            };

            this.websocket.onmessage = (msg) => {
                const data = JSON.parse(msg.data);
                try {
                    this.handle_event(data);
                } catch (ex) {
                    console.error(ex, "\nwhile processing", data);
                }
            };
        });
    }
}


Bridge.string = (default_val) => {
    return {
        type: 'string',
        default: default_val,
    };
}


module.exports = { Bridge };